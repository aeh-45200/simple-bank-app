package Models;

import Enums.OperationType;
import Repositories.AccountsRepository;
import Repositories.ClientsRepository;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Operation implements Serializable {
    private int id;
    private int accountId;
    private OperationType operation;
    private double amount;
    private Integer targetAccountId;
    private Date date;

    public Operation(int accountId, OperationType operation, double amount, Integer targetAccountId, Date date) {
        this.accountId = accountId;
        this.operation = operation;
        this.amount = amount;
        this.targetAccountId = targetAccountId;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public OperationType getOperation() {
        return operation;
    }

    public void setOperation(OperationType operation) {
        this.operation = operation;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Integer getTargetAccountId() {
        return targetAccountId;
    }

    public void setTargetAccountId(Integer targetAccountId) {
        this.targetAccountId = targetAccountId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formattedDate = formatter.format(this.getDate());
        Client client = this.getClient(this.getAccountId());
        String operation = "Klient: " + client.getFirstName() + " " + client.getLastName() +
                ", Operacja: " + this.getOperationText() +
                ", Kwota: " + String.format("%.2f", amount) + " zł";
        if (targetAccountId != null) {
            Client targetClient = this.getClient(targetAccountId);
            operation += ", Odbiorca: " + targetClient.getFirstName() +
                    " " + targetClient.getLastName();
        }
        operation += ", Data operacji: " + formattedDate;
        return operation;
    }

    private Client getClient(int accountId) {
        AccountsRepository accountsRepository = new AccountsRepository();
        ClientsRepository clientsRepository = new ClientsRepository();
        Account account = accountsRepository.find(accountId);
        return clientsRepository.find(account.getClientId());
    }

    private String getOperationText() {
        String operationText = "";
        switch (this.getOperation()) {
            case DEPOSIT:
                operationText = "Wpłata";
                break;
            case WITHDRAWAL:
                operationText = "Wypłata";
                break;
            case TRANSFER:
                operationText = "Przelew";
                break;
        }
        return operationText;
    }
}
