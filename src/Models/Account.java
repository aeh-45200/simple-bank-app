package Models;

import java.io.Serializable;
import java.util.Random;

public class Account implements Serializable {
    private int id;
    private int clientId;
    private String accountNumber;
    private double balance;

    public Account(int clientId) {
        this.setBalance(0);
        this.setAccountNumber(this.generateAccountNumber());
        this.setClientId(clientId);
    }

    private String generateAccountNumber() {
        StringBuilder accountNumber = new StringBuilder("PL");
        Random random = new Random();
        accountNumber.append(random.nextInt(9));
        accountNumber.append(random.nextInt(9));
        for (int i = 0; i < 24; i++) {
            if (i % 4 == 0) {
                accountNumber.append(" ");
            }
            accountNumber.append(random.nextInt(9));
        }
        return accountNumber.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String toString()
    {
        return "Numer konta: " + this.getAccountNumber()
                + ", Kwota " + String.format("%.2f", this.getBalance()) + " zł";
    }
}
