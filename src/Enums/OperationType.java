package Enums;

public enum OperationType {
    DEPOSIT, WITHDRAWAL, TRANSFER
}
