import Enums.OperationType;
import Models.Account;
import Models.Client;
import Models.Operation;
import Repositories.AccountsRepository;
import Repositories.ClientsRepository;
import Repositories.OperationsRepository;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    private final ClientsRepository clientsRepository;
    private final AccountsRepository accountsRepository;
    private final OperationsRepository operationsRepository;

    public App() {
        this.clientsRepository = new ClientsRepository();
        this.accountsRepository = new AccountsRepository();
        this.operationsRepository = new OperationsRepository();
    }

    public void mainMenu() {
        this.displayMenu();
        Scanner scanner = new Scanner(System.in);
        try {
            int selectedOption = scanner.nextInt();
            switch (selectedOption) {
                case 1:
                    this.displayClients();
                    break;
                case 2:
                    this.addClient();
                    break;
                case 3:
                    this.displayOperations();
                    break;
                case 9:
                    this.help();
                case 0:
                    return;
                default:
                    System.out.println("Nie prawidłowa opcja");
                    this.mainMenu();
                    break;
            }
        } catch (InputMismatchException e) {
            System.out.println("Nieprawidłowa opcja.");
            this.mainMenu();
        }
    }

    private void displayOperations() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Lista operacji");
        System.out.println("Zostaw puste daty jeżeli chcesz wyświetlić wszystkie operacje");
        System.out.println("Podaj datę w formacie rok-miesiąc-dzień godzina:minuta");
        System.out.print("Data początkowa: ");
        String dateFromText = scanner.nextLine();
        System.out.print("Data końcowa: ");
        String dateToText = scanner.nextLine();
        LocalDateTime dateFrom = null;
        LocalDateTime dateTo = null;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            if (dateFromText != "") {
                dateFrom = LocalDateTime.parse(dateFromText, formatter);
            }
            if (dateToText != "") {
                dateTo = LocalDateTime.parse(dateToText, formatter);
            }
        } catch (DateTimeParseException exception) {
            System.out.println(exception.getMessage());
            System.out.println("Nieprawidłowy format. Podaj datę w formacie rok-miesiąc-dzień godzina:minuta");
            this.displayOperations();
        }
        ArrayList<Operation> operations = this.operationsRepository.findAll();
        for (Operation operation : operations) {
            LocalDateTime operationDate = operation.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            if ((dateFrom == null || !operationDate.isBefore(dateFrom))
                    && (dateTo == null || !operationDate.isAfter(dateTo))) {
                System.out.println(operation);
            }
        }

        System.out.println("1) Szukaj ponownie");
        System.out.println("0) Cofnij");
        System.out.print("Wybierz opcję: ");
        int option = scanner.nextInt();
        if (option == 1) {
            this.displayOperations();
        } else {
            this.mainMenu();
        }
    }

    public void displayMenu() {
        System.out.println("-----Klienci-----");
        System.out.println("1) lista");
        System.out.println("2) dodaj");

        System.out.println("\n-----Transakcje-----");
        System.out.println("3) lista");

        System.out.println("\n----");
        System.out.println("9) Pomoc");
        System.out.println("0) Wyjdź");
        System.out.print("Podaj liczb od 1 do 4 aby przejść dalej lub 0 aby wyjść: ");
    }

    public void displayClients() {
        ArrayList clients = this.clientsRepository.findAll();
        if (!clients.isEmpty()) {

            System.out.println("Lista klientów:");
            clients.forEach(client -> {
                System.out.println(client);
            });
            System.out.print("Wybierz użytkownika: ");
            Scanner scanner = new Scanner(System.in);
            int clientId = scanner.nextInt();
            if (clientId != 0) {
                this.displayClientOptions(clientId);
            } else {
                this.mainMenu();
            }
        } else {
            System.out.println("Brak klientów na liście.");
            this.mainMenu();
        }
    }

    private void addClient() {
        Scanner scanner = new Scanner(System.in);
        Client client = new Client();
        client.setId(clientsRepository.getNextId());
        System.out.println("Dodaj klienta");
        System.out.print("Podaj imię: ");
        client.setFirstName(scanner.nextLine());
        System.out.print("Podaj nazwisko: ");
        client.setLastName(scanner.nextLine());
        System.out.print("Podaj pesel: ");
        client.setPesel(scanner.nextLine());
        this.clientsRepository.add(client);

        Account account = new Account(client.getId());
        account.setId(this.accountsRepository.getNextId());
        this.accountsRepository.add(account);
        System.out.println("Dane klienta zostały zapisne");
        this.mainMenu();
    }

    private void displayClientOptions(int clientId) {
        Client client = this.clientsRepository.find(clientId);
        Account account = this.accountsRepository.findBy("clientId", clientId);
        System.out.printf("Dane klienta: %s \n", client.toString());
        System.out.printf("Dane konta: %s \n", account.toString());

        System.out.println("1) Edytuj");
        System.out.println("2) Usuń");
        System.out.println("3) Wpłać");
        System.out.println("4) Wypłać");
        System.out.println("5) Przelew");
        System.out.println("0) Cofnij");
        Scanner scanner = new Scanner(System.in);
        int operation = scanner.nextInt();
        switch (operation) {
            case 1:
                this.editClient(clientId);
                break;
            case 2:
                this.removeClient(clientId);
                break;
            case 3:
                this.depositOnAccount(clientId, account);
                break;
            case 4:
                this.withdrawalFromAccount(clientId, account);
                break;
            case 5:
                this.transfer(clientId, account);
                break;
            case 0:
                this.displayClients();
                break;
            default:
                System.out.println("Nieprawidłowa opcja");
                this.displayClientOptions(clientId);
                break;
        }
    }

    private void depositOnAccount(int clientId, Account account) {
        System.out.print("Podaj kwotę: ");
        Scanner scanner = new Scanner(System.in);
        double amount = 0;
        try {
            amount = Double.parseDouble(scanner.nextLine());
        } catch (Exception e) {
            System.out.println("Nieprawidłow kwota. W przypadku liczb zmiennoprzecinkowych użyj '.' jako separator liczb dziesiętnych");
            this.withdrawalFromAccount(clientId, account);
        }
        if (amount > 0) {
            account.setBalance(account.getBalance() + amount);
            this.accountsRepository.update(account.getId(), account);
            System.out.println("Kwota została przypisana do konta");
            Date currentDate = new Date();
            Operation operation = new Operation(account.getId(), OperationType.DEPOSIT, amount, null, currentDate);
            this.operationsRepository.add(operation);
            this.displayClientOptions(clientId);
        } else {
            System.out.println("Nieprawidłowa kwota");
            this.depositOnAccount(clientId, account);
        }
    }

    private void withdrawalFromAccount(int clientId, Account account) {
        if (account.getBalance() > 0) {
            System.out.print("Podaj kwotę: ");
            Scanner scanner = new Scanner(System.in);
            double amount = 0;
            try {
                amount = Double.parseDouble(scanner.nextLine());
            } catch (Exception e) {
                System.out.println("Nieprawidłow kwota. W przypadku liczb zmiennoprzecinkowych użyj '.' jako separator liczb dziesiętnych");
                this.withdrawalFromAccount(clientId, account);
            }
            if (amount > 0 && amount < account.getBalance()) {
                account.setBalance(account.getBalance() - amount);
                this.accountsRepository.update(account.getId(), account);
                System.out.println("Kwota została zmieniona");
                Date currentDate = new Date();
                Operation operation = new Operation(account.getId(), OperationType.WITHDRAWAL, amount, null, currentDate);
                this.operationsRepository.add(operation);
                this.displayClientOptions(clientId);
            } else {
                System.out.println("Nieprawidłowa kwota. Kwota musi być pomiędzy 0.01 a " + String.format("%.2f", account.getBalance()) + " zł");
                this.withdrawalFromAccount(clientId, account);
            }
        } else {
            System.out.println("Brak środków na koncie.");
            this.displayClientOptions(clientId);
        }
    }

    private void editClient(int clientId) {
        Scanner scanner = new Scanner(System.in);
        Client client = this.clientsRepository.find(clientId);
        System.out.println("Edytuj klienta");
        System.out.println("Zostaw pustą linię jeżeli nie chcesz zmieniać informacji.");

        System.out.print("Podaj imię: ");
        String firstName = scanner.nextLine();
        if (firstName != "") {
            client.setFirstName(firstName);
        }
        System.out.print("Podaj nazwisko: ");
        String lastName = scanner.nextLine();
        if (lastName != "") {
            client.setLastName(lastName);
        }
        System.out.print("Podaj pesel: ");
        String pesel = scanner.nextLine();
        if (pesel != "") {
            client.setPesel(pesel);
        }
        this.clientsRepository.update(clientId, client);
        System.out.println("Dane klienta zostały zapisne");
        this.mainMenu();
    }

    private void removeClient(int clientId) {
        this.clientsRepository.remove(clientId);
        System.out.println("Klient został usunięty");
        this.displayClients();
    }

    private void transfer(int clientId, Account account) {
        if (account.getBalance() > 0) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Podaj kwotę: ");
            double amount = Double.parseDouble(scanner.nextLine());
            System.out.print("Podaj numer konta: ");
            String accountNumber = scanner.nextLine();
            Account targetAccount = this.accountsRepository.findBy("accountNumber", accountNumber);
            if (targetAccount != null) {
                if (amount > 0 && amount < account.getBalance()) {
                    account.setBalance(account.getBalance() - amount);
                    this.accountsRepository.update(account.getId(), account);
                    targetAccount.setBalance(targetAccount.getBalance() + amount);
                    this.accountsRepository.update(targetAccount.getId(), targetAccount);
                    System.out.println("Wykonano przelew pomiędzy kontami");
                    Date currentDate = new Date();
                    Operation operation = new Operation(account.getId(), OperationType.TRANSFER, amount, targetAccount.getId(), currentDate);
                    this.operationsRepository.add(operation);
                    this.displayClientOptions(clientId);
                } else {
                    System.out.println("Nieprawidłowa kwota. Kwota musi być pomiędzy 0.01 a " + String.format("%.2f", account.getBalance()) + " zł");
                    this.transfer(clientId, account);
                }
            } else {
                System.out.println("Nie znaleziono konta z takim numerem");
                this.transfer(clientId, account);
            }
        } else {
            System.out.println("Brak środków na koncie.");
            this.displayClientOptions(clientId);
        }
    }

    private void help() {
        System.out.println("Pomoc");
        System.out.println("----- Główne Menu -----");
        System.out.println("1) lista: Wyświetla listę wszystkich klientów.");
        System.out.println("2) dodaj: Dodaje nowego klienta do bazy danych.");
        System.out.println("3) lista: Wyświetla listę wszystkich operacji bankowych.");
        System.out.println("9) Pomoc: Wyświetla sekcję pomocy.");
        System.out.println("0) Wyjdź: Zamyka aplikację.");

        System.out.println("\n----- Lista Klientów -----");
        System.out.println("Po wyborze opcji 1) lista:");
        System.out.println("Wybierz użytkownika: Wybierz ID klienta, aby zobaczyć dostępne dla niego opcje.");

        System.out.println("\n----- Dodanie Klienta -----");
        System.out.println("Po wyborze opcji 2) dodaj:");
        System.out.println("Podaj imię: Wprowadź imię nowego klienta.");
        System.out.println("Podaj nazwisko: Wprowadź nazwisko nowego klienta.");
        System.out.println("Podaj pesel: Wprowadź numer PESEL nowego klienta.");

        System.out.println("\n----- Opcje Klienta -----");
        System.out.println("Dostępne po wyborze klienta z listy:");
        System.out.println("1) Edytuj: Edytuje dane wybranego klienta.");
        System.out.println("2) Usuń: Usuwa wybranego klienta z bazy danych.");
        System.out.println("3) Wpłać: Dokonuje wpłaty na konto wybranego klienta.");
        System.out.println("4) Wypłać: Dokonuje wypłaty z konta wybranego klienta.");
        System.out.println("5) Przelew: Wykonuje przelew z konta wybranego klienta.");
        System.out.println("0) Cofnij: Wraca do listy klientów.");

        System.out.println("\n----- Wpłata na Konto -----");
        System.out.println("Podaj kwotę: Wprowadź kwotę do wpłaty. W przypadku liczb " +
                "zmiennoprzecinkowych użyj '.' jako seperatura dziesiętnego.");

        System.out.println("\n----- Wypłata z Konta -----");
        System.out.println("Podaj kwotę: Wprowadź kwotę do wypłaty. W przypadku liczb " +
                "zmiennoprzecinkowych użyj '.' jako seperatura dziesiętnego");

        System.out.println("\n----- Przelew -----");
        System.out.println("Podaj kwotę: Wprowadź kwotę do przelewu. W przypadku liczb " +
                "zmiennoprzecinkowych użyj '.' jako seperatura dziesiętnego.");
        System.out.println("Podaj numer konta: Wprowadź numer konta odbiorcy.");

        System.out.println("\n----- Edycja Klienta -----");
        System.out.println("Podaj imię: Wprowadź nowe imię lub pozostaw puste, aby nie zmieniać.");
        System.out.println("Podaj nazwisko: Wprowadź nowe nazwisko lub pozostaw puste, aby nie zmieniać.");
        System.out.println("Podaj pesel: Wprowadź nowy PESEL lub pozostaw puste, aby nie zmieniać.");

        System.out.println("\n----- Usuwanie Klienta -----");
        System.out.println("Klient jest usuwany bez dodatkowych pytań.");

        System.out.println("\n----- Lista Operacji -----");
        System.out.println("Data początkowa: Wprowadź datę początkową w formie \"rok-miesiąc-dzień godzina:minuta\" lub zostaw puste.");
        System.out.println("Data końcowa: Wprowadź datę końcową w formie \"rok-miesiąc-dzień godzina:minuta\" lub zostaw puste.");

        System.out.println("\n----- Pomoc -----");
        System.out.println("Wyświetla tę sekcję pomocy.");

        System.out.println("\n----- Wyjście -----");
        System.out.println("Zamyka aplikację.");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Naciśnij ENTER aby wrócić do menu głównego");
        scanner.nextLine();
    }
}
