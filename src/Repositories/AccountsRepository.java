package Repositories;

import Models.Account;
import Models.Client;

public class AccountsRepository extends CrudRepository<Account> {
    public AccountsRepository() {
        super("data/accounts.bin");
    }
}
