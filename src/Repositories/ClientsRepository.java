package Repositories;

import Models.Client;

public class ClientsRepository extends CrudRepository<Client> {
    public ClientsRepository() {
        super("data/clients.bin");
    }
}
