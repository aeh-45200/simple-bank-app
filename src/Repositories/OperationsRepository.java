package Repositories;

import Models.Operation;

public class OperationsRepository extends CrudRepository<Operation> {
    public OperationsRepository() {
        super("data/operations.bin");
    }
}
