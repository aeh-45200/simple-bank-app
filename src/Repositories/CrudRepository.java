package Repositories;


import Models.Account;
import Models.Client;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class CrudRepository<T> {
    private final String fileName;

    public CrudRepository(String fileName) {
        this.fileName = fileName;
    }

    public void add(T object) {
        ArrayList<T> list = this.findAll();
        list.add(object);
        try {
            File directory = new File("data");
            if (!directory.exists()) {
                directory.mkdir();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(this.fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(list);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            System.out.println("Nie udało się zapisać danych. " + e.getMessage());
        }
    }

    public ArrayList<T> findAll() {
        ArrayList<T> list = new ArrayList<T>();
        try {
            File file = new File(this.fileName);
            if (file.exists()) {
                FileInputStream fileInputStream = new FileInputStream(this.fileName);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                list = (ArrayList<T>) objectInputStream.readObject();
                objectInputStream.close();
                fileInputStream.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return list;
    }


    public void removeAll() {
        ArrayList<T> list = new ArrayList<>();
        this.writeToBinFile(list);
    }

    private void writeToBinFile(ArrayList<T> list) {
        try {
            File directory = new File("data");
            if (!directory.exists()) {
                directory.mkdir();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(this.fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(list);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            System.out.println("Nie udało się zapisać danych. " + e.getMessage());
        }
    }

    public int getNextId() {
        return this.findAll().stream().mapToInt(element -> {
            try {
                Field idField = element.getClass().getDeclaredField("id");
                idField.setAccessible(true);
                return (Integer) idField.get(element);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
                return 0;
            }
        }).max().orElse(0) + 1;
    }

    public T find(int rowId) {
        return this.findAll().stream().filter(element -> {
                    try {
                        Field idField = element.getClass().getDeclaredField("id");
                        idField.setAccessible(true);
                        int elementId = (Integer) idField.get(element);
                        return elementId == rowId;
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .findFirst()
                .orElse(null);
    }

    public void update(int rowId, T model) {
        ArrayList<T> list = this.findAll();
        for (int i = 0; i < list.size(); i++) {
            T element = list.get(i);
            try {
                Field idField = element.getClass().getDeclaredField("id");
                idField.setAccessible(true);
                int elementId = (Integer) idField.get(element);
                if (elementId == rowId) {
                    list.set(i, model);
                    break;
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        this.writeToBinFile(list);
    }

    public void remove(int rowId)
    {
        ArrayList<T> list = this.findAll();
        for (int i = 0; i < list.size(); i++) {
            T element = list.get(i);
            try {
                Field idField = element.getClass().getDeclaredField("id");
                idField.setAccessible(true);
                int elementId = (Integer) idField.get(element);
                if (elementId == rowId) {
                    list.remove(rowId);
                    break;
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        this.writeToBinFile(list);
    }

    public T findBy(String fieldName, Object value) {
        return this.findAll().stream().filter(element -> {
                    try {
                        Field field = element.getClass().getDeclaredField(fieldName);
                        field.setAccessible(true);
                        Object fieldValue = field.get(element);
                        return fieldValue.equals(value);
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                        return false;
                    }
                })
                .findFirst()
                .orElse(null);
    }
}