Napisać aplikację „Proste operacje bankowe”:

- aplikacja ma przechowywać informacje o 
- klientach
- kontach (dla uproszczenia założyć że klient ma dokładnie jedno konto)
- transakcjach (wpłata, wypłata, przelew pomiędzy klientami tego banku)
- aplikacja ma umożliwiać przeglądanie informacji o koncie, saldo konta, transakcji z określonego okresu

Aplikacja ma posiadać interfejs tekstowy i przechowywać dane w plikach binarnych.